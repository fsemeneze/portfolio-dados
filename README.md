# Portfólio de Projetos em Dados
Bem-vindo ao meu portfólio de análise de dados! Este repositório contém uma coleção dos meus projetos de análise de dados, nos quais demonstro minhas habilidades técnicas e experiência em trabalhar com dados.

## Projetos

| Nome do Projeto | Notebook ou Link	 | Linguagem  | Observação |
| ------- | --------- | ------- | --------- |
| Analise de clientes inadimplentes   | [Analise de clientes inadimplentes](https://github.com/fsemeneze/portfolio/tree/792a74523e8e40e03e63ee7779384ec3bafed4bf/Analise%20de%20clientes%20inadimplentes) | Python  | Um projeto de insight da definição do problema, passando pelo estagio de processamento e limpeza dos dados, análise exploratória e comunicação dos resultados gerados |
| Analise de vendas Petshop | [Alura Pets](https://app.powerbi.com/view?r=eyJrIjoiMjcyYjMxYmYtMjdjNy00ZDkzLWE3N2ItMDZlNzhhZTg3M2JhIiwidCI6IjFlMWM5MzI3LWZhOTUtNDJjNS1hMGFkLWExNGE1ODMwOTMzMCJ9) | Power BI  | Este dashboard fornece uma visão abrangente das vendas de uma empresa de Petshop ao longo de um determinado período. Ele foi projetado para ajudar a empresa a analisar e monitorar o desempenho das vendas, identificar padrões e tomar decisões estratégicas. (Criado a partir do curso de Power BI Desktop da Alura) |

## Contato

Se você tiver alguma dúvida ou desejar entrar em contato comigo, pode enviar uma mensagem para o meu [linkedin](https://www.linkedin.com/in/fabriciosmz/).

Obrigado por visitar meu portfólio de análise de dados! Espero que você encontre meu trabalho interessante e informativo.
